import matplotlib.pyplot as plt
from analysis import *

def plot_water_levels(station, dates, levels):
	plt.plot(dates, levels)
	
	plt.xlabel('date')
	plt.ylabel('water level (m)')
	
	plt.xticks(rotation=45);
	plt.title(station.name)
	
	plt.tight_layout()

	plt.show()
	
	return

def plot_water_level_with_fit(station, dates, levels, p):
	plt.plot(dates, levels)
	
	poly = polyfit(dates, levels, p)
	
	date_floats = [matplotlib.dates.date2num(i) for i in dates]
	xs = np.linspace(date_floats[0], date_floats[len(date_floats)-1], 30)
	plt.plot(xs, poly(xs))
	
	plt.plot([date_floats[0], date_floats[len(date_floats)-1]], [station.typical_range[0], station.typical_range[0]])
	plt.plot([date_floats[0], date_floats[len(date_floats)-1]], [station.typical_range[1], station.typical_range[1]])
	
	plt.xlabel('date')
	plt.ylabel('water level (m)')
	
	plt.xticks(rotation=45);
	plt.title(station.name)

	plt.tight_layout()
	
	plt.show()
	
	return