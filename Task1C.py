from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list


Cambridge_City_Centre = (52.2053, 0.1218) 
print(stations_within_radius(build_station_list(), Cambridge_City_Centre, 10))