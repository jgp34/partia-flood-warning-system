from enum import Enum
from floodsystem.stationdata import *
from flood import *
from floodsystem.datafetcher import *
import numpy as np
import matplotlib.dates


def town_risk_levels(history_length=3):
	
	# limiting length of history calculated over to 5 days
	if history_length > 5:
		print("Time period input is too large and will cause the function to be very slow; continuing with maximum time period of 5 days.")
		history_length = 5
	
	# getting the list of Monitoring Station instances
	stations = build_station_list()
	
	# updating the water level data for all the Monitoring Station instances
	update_water_levels(stations)
	
	# building a list of the Monitoring Station instances that valid water level data
	valid_stations = []
	low_risk_stations = []
	possible_risk_stations = []
	for i in stations:
		if i.typical_range_consistent() == True:
			valid_stations.append(i)
			i.risk = 0
		else:
			i.risk = None

	# finding all Monitoring Station instances that have a water level of some risk
	for i in valid_stations:
		level = i.relative_water_level()[1]
		if level != None:
			if level < 1:
				low_risk_stations.append(i)
				i.risk = 0
			else:
				possible_risk_stations.append(i)
	
	# splitting the stations with some risk into low, mid and high level
	low_level_stations = []
	high_level_stations = []
	mid_level = possible_risk_stations[int(len(possible_risk_stations)/2)].relative_water_level()[1]
	for i in possible_risk_stations:
		level = i.relative_water_level()[1]
		if 1 <= level < mid_level:
			low_level_stations.append(i.name)
			i.risk += 1
		else:
			high_level_stations.append(i.name)
			i.risk += 2
	
	# fitting linear trend lines to the recent water level readings of al stations at risk
	station_coefficients = []
	for i in possible_risk_stations:
		try:	
			dates, levels = fetch_measure_levels(i.measure_id, datetime.timedelta(days=history_length))
			date_floats = [matplotlib.dates.date2num(k) for k in dates]
			coefficients = np.polyfit(date_floats, levels, 1)
			station_coefficients.append((i, coefficients[0]))
		except Exception:
			station_coefficients.append((i, None))

	# incrementing station risk levels according to the gradient of the linear trend line
	mid_grad = station_coefficients[int(len(station_coefficients)/2)][1]
	low_grad_stations = []
	high_grad_stations = []
	for i in station_coefficients:
		if i[1] == None:
			i[0].risk = None
		elif i[1] < mid_grad:
			low_grad_stations.append(i[0].name)
			i[0].risk += 0
		elif i[1] >= mid_grad:
			high_grad_stations.append(i[0].name)
			i[0].risk += 1
		else:
			i[0].risk = None

	# finding stations with invalid level history data or gradient
	invalid_data = []
	for i in valid_stations:
		if i.risk == None:
			invalid_data.append(i.name)

	# transferring station risk data to the nearby towns
	town_risks = {}
	for i in possible_risk_stations:
		if i.town in town_risks.keys():
			town_risk = town_risks[i.town][0]
			number_of_stations = town_risks[i.town][1]
			town_risks[i.town] = [town_risk + i.risk, number_of_stations + 1]
		else:
			town_risks[i.town] = [0, 0]
	
	# diving by the number of stations at each town to find average risk at each town
	true_town_risks = {}
	for key in town_risks.keys():
		#	True risk = average risk
		if town_risks[key][0] != 0:
			true_risk = town_risks[key][0]/town_risks[key][1]
		else:
			true_risk = 0
		true_town_risks[key] = true_risk
	
	# distributing towns to named categories according to risk level
	low_town_risk = []
	moderate_town_risk = []
	high_town_risk = []
	severe_town_risk = []
	for key in true_town_risks.keys():
		risk = true_town_risks[key]
		if risk == 0:
			low_town_risk.append(key)
		elif risk == 1:
			moderate_town_risk.append(key)
		elif risk == 2:
			high_town_risk.append(key)
		elif risk == 3:
			severe_town_risk.append(key)
		else:
			pass
	
	# printing all gathered information
	print("""

Flood severity ratings:

Invalid data - invalid relative water level or invalid gradient of water level from past """, history_length, """days

Low risk - relative water level below typical high

Moderate risk - level below that of the station halfway between relative water level of 1 and the maximum
and gradient below that of the station halfway between the maximum and minimum gradients

High risk - level above that of the station halfway between relative water level of 1 and the maximum
or gradient above that of the station halfway between the maximum and minimum gradients

Severe risk - level above that of the station halfway between relative water level of 1 and the maximum
and gradient above that of the station halfway between the maximum and minimum gradients


Towns with invalid data:
""", invalid_data, """

Towns with low flood risk:
""", low_town_risk, """

Towns with moderate flood risk:
""", moderate_town_risk, """

Towns with high flood risk:
""", high_town_risk, """

Towns with severe flood risk:
""", severe_town_risk)

	return

# running the function across the default value of the last 3 days
town_risk_levels()