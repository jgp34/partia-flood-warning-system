from plot import *
from floodsystem.stationdata import *
from flood import *

stations = build_station_list()

current_highest = [i[0] for i in stations_highest_rel_level(stations, 5)]

for i in current_highest:
	dates, levels = fetch_measure_levels(i.measure_id, datetime.timedelta(days=2))
	plot_water_level_with_fit(i, dates, levels, 4)