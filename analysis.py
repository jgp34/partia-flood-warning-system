from floodsystem.datafetcher import *
import matplotlib
import numpy as np

def polyfit(dates, levels, p):
	date_floats = [matplotlib.dates.date2num(i) for i in dates]
	poly = np.poly1d(np.polyfit(date_floats, levels, p))
	return poly