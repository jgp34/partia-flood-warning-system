# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
	"""This class represents a river level monitoring station"""
    
	# constructor:
	def __init__(self, station_id, measure_id, label, coord, typical_range, river, town):
    
		self.station_id = station_id
		self.measure_id = measure_id
        
		# Handle case of erroneous data where data system returns
		# '[label, label]' rather than 'label'
		self.name = label
		if isinstance(label, list):
			self.name = label[0]
            
		self.coord = coord
		self.typical_range = typical_range
		self.river = river
		self.town = town
		
		self.risk = None
		
		self.latest_level = None
		
    # representation:
	def __repr__(self):
		d = "Station name:     {}\n".format(self.name)
		d += "   id:            {}\n".format(self.station_id)
		d += "   measure id:    {}\n".format(self.measure_id)
		d += "   coordinate:    {}\n".format(self.coord)
		d += "   town:          {}\n".format(self.town)
		d += "   river:         {}\n".format(self.river)
		d += "   typical range: {}\n".format(self.typical_range)
		d += "   risk:          {}".format(self.risk)
		return d
		
	def typical_range_consistent(self):
		# return a Bool for whether the data is consistent
		if isinstance(self.typical_range, tuple):
			if isinstance(self.typical_range[0], float) and isinstance(self.typical_range[1], float):
				if (self.typical_range)[1] < (self.typical_range)[0]:
					return False
				else:
					return True
			else:
				return False
		else:
			return False

	def relative_water_level(self):
		if self.typical_range_consistent() and isinstance(self.latest_level, float):
			typical_low = self.typical_range[0]
			typical_high = self.typical_range[1]
			relative = (self.latest_level - typical_low)/(typical_high - typical_low)
			return (self.name, relative)
		else:
			return (self.name, None)

def inconsistent_typical_range_stations(stations):
	# takes a List of Monitoring Station instances
	# returns a List of Monitoring Station instances that have lacking/inconsistent attributes
	output = []
	for i in stations:
		if i.typical_range_consistent() == False:
			output.append(i)
	
	return output