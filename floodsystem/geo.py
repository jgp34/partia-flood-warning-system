# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

import numpy as np
from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import *
import itertools

def hav(theta):
	# haversine function; takes a Float or Int
	# returns a Float
    return 0.5 - 0.5*np.cos(theta)

def degtorad(theta):
	# takes a Float or Int in degrees
	# returns that angle in radians as a Float
	if(isinstance(theta, (int, float))):
		return theta * 2 * np.pi / 360
	else:
		raise TypeError
		return 0

def distance(latlong1, latlong2):
    # returns the distance between two Tuples of Floats - latitude and longitude (in degrees) in km
    # (12742 is diameter of earth in km)
   return 12742*np.arcsin((hav(degtorad(latlong2[0] - latlong1[0])) + np.cos(degtorad(latlong1[0]))*np.cos(degtorad(latlong2[0]))*hav(degtorad(latlong2[1] - latlong1[1])))**0.5)

def stations_by_distance(stations, p):
    # takes a List of Monitoring Station instances and a Tuple of Floats - the latitude/longitude of a point
    # returns a List, sorted by distance, of Tuples of (Monitoring Station, Float distance to the point in km)
    output = []
    for i in range(len(stations)):
        to_add = (stations[i], distance(stations[i].coord, p))
        output.append(to_add)

    output = sorted_by_key(output, 1)
    return output

def stations_within_radius(monitoring_stations, centre, r):
    # takes a List of Monitoring Station instances, a Tuple of Floats - latitude & longitude (in degrees) of a point and Float radius in km
    # returns a List of Monitoring Station instances that are within the radius of the point
    stations_distance = stations_by_distance(monitoring_stations, centre)
    output = []
    for i in range(len(stations_distance)):
        if stations_distance[i][1] <= r:
            output.append(stations_distance[i][0].name)
        else:
            pass
    #output = sorted(output)
    return output

def rivers_with_stations(stations):
    # takes a List of Monitoring Station instances
    # returns a Set of Strings - names of rivers which these stations are on
    output = set()
    for i in range(len(stations)):
        if stations[i].river not in output:
            output.add(stations[i].river)

    return output

def stations_by_river(stations, coord=False): #, river, coord=False
	# takes a List of Monitoring Station instances
	# if coord is not given / given as False:
		# returns a Dict mapping Strings - names of rivers - to Lists of Monitoring Station instances - the stations on each river
	# if coord is given as True:
		# returns a Dict mapping Strings - names of rivers - to Lists of Tuples of (Monitoring Station, Tuple of Floats) - the stations on each river, each with its coordinates
		# this is used to find adjacent stations on the river to predict water levels downstream in Extension.py
	rivers = list(rivers_with_stations(stations))
	output = {}
	for i in rivers:
		output[i] = []
		
	if False:
		for i in stations:
			output[i.river].append(i)
						
	else:
		for i in stations:
			output[i.river].append((i, i.coord))
			
	return output
	
def rivers_by_station_number(stations, N):
	# takes a List of Monitoring Station instances and an Int
	# returns a List of (String, Int) Tuples - the rivers with the N largest number of stations on them and, and those numbers
	all_rivers_by_station = []
	rivers = list(rivers_with_stations(stations))
	sbr = stations_by_river(stations)
	for i in rivers:
		all_rivers_by_station.append((i, len(sbr[i])))
	
	all_rivers_by_station.sort(key = lambda x: x[1], reverse = True)
	output = all_rivers_by_station[0:N]
	n = N
	while all_rivers_by_station[n][1] == all_rivers_by_station[N-1][1]:
		output.append(all_rivers_by_station[n])
		n += 1
	
	return output
