from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

# get list of Monitoring_Station objects:
stations = build_station_list()

# make a sorted list of distances between each station and a longitude and latitude:
Cambridge_City_Centre = (52.2053, 0.1218)
by_distance = stations_by_distance(stations, Cambridge_City_Centre)

# find the first ten and last ten of the sorted list:
without_towns = by_distance[:10] + by_distance[-10:]

# creates list of station names, towns and distances for first/last ten list
with_towns = []
for i in range(20):
    with_towns.append((without_towns[i][0].name, without_towns[i][0].town, without_towns[i][1]))

print(with_towns)
