import numpy as np
from floodsystem.datafetcher import *
from floodsystem.geo import *
from floodsystem.stationdata import *
from floodsystem.utils import *

def stations_on_a_river_with_coord(stations, river):
    stations_on_river = stations_by_river(stations, coord=True)[river]
    stations_on_river_2 = []
    for i in range(len(stations_on_river)):
        stations_on_river_2.append((stations_on_river[i][0].name, stations_on_river[i][1]))
    return stations_on_river_2






station_list = build_station_list()
print(stations_on_a_river_with_coord(station_list, station_list[20].river))
