from floodsystem.geo import *
from floodsystem.stationdata import build_station_list
from floodsystem.station import *

stations = inconsistent_typical_range_stations(build_station_list())
stations_names = []
for i in stations:
	stations_names.append(i.name)

print(sorted(stations_names))