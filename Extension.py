import numpy as np
from floodsystem.datafetcher import *
from floodsystem.geo import *
from floodsystem.stationdata import *
from floodsystem.utils import *

# i think this function effectively does what 'stations_by_river' was supposed to do and now already does
def all_stations_by_river(stations):
    stations_on_rivers = []
    all_rivers = list(rivers_with_stations(stations))
    sorted_rivers = sorted(all_rivers, key=str.lower)
    for i in range(len(rivers_with_stations(stations))):
        river_i = (sorted_rivers)[i]
        stations_on_rivers  += [[river_i, stations_by_river(stations, river_i, coord=True)]]
    return stations_on_rivers

def stations_along_river(stations_by_rivers):
    for i in range(len(stations_by_rivers)):
        river_i = stations_by_rivers[i][0]
        stations_on_i = stations_by_rivers[i][1]
        ordered_stations_on_i = []
        closest_stations = []
        adjacent_stations_by_station = []
        for j in range(stations_on_i):
            station_j = stations_on_i[j]
            station_j_distances = []
            for k in range(stations_on_i-1):
                coord_station_kplus1 = stations_on_i[k+1][1]
                station_j_distances.append([stations_on_i[k+1], distance(station_j[1], coord_station_kplus1), coord_station_kplus1])
            station_j_sorted_distances = sorted_by_key(station_j_distances, 1)
            closest_stations_to_j = station_j_sorted_distances[:2]
            closest_stations.append([station_j, closest_stations_to_j])
        for n in range(len(stations_on_i)):
            station_n = closest_stations[n]
            closest_station_to_n = station_n[1][0]
            if station_n[1][1] == closest_station_to_n[1][0] or closest_station_to_n[1][1]:
                station_n.remove(station_n[1][1])
                adjacent_to_n = [closest_station_to_n[0]]
            else:
                adjacent_to_n = [closest_station_to_n[0], station_n[1][1][0]]
            adjacent_stations_by_station.append([station_n[0], adjacent_to_n])
        


all_stations_and_rivers = all_stations_by_river(build_station_list())
print(all_stations_and_rivers)
	