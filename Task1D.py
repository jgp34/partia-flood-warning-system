from floodsystem.geo import *
from floodsystem.stationdata import build_station_list

stations = build_station_list()

station_objects_by_river = stations_by_river(stations)

def dict_obj_to_name(dict):
	output = {}
	for river, list in dict.items():
		output[river] = []
		for j in list:
			output[river].append(j[0].name)
			
	return output

station_names_by_river = dict_obj_to_name(station_objects_by_river)

print(sorted(station_names_by_river['River Aire']))
print(sorted(station_names_by_river['River Cam']))
print(sorted(station_names_by_river['River Thames']))