from floodsystem.geo import *
import unittest

def test_hav():
    hav_pi = 1
    assert hav_pi == hav(np.pi)

def test_detograd():
    deg_to_rad_180 = np.pi
    assert deg_to_rad_180 == degtorad(180)

def test_distance():
    distance_origin_to_origin = 0
    assert distance_origin_to_origin == distance((0, 0), (0,0))
    assert isinstance(distance((0, 0), (0,0)), float)

def test_stations_by_distance():
    stations_by_distance_output = stations_by_distance(build_station_list(), (0,0))
    assert isinstance(stations_by_distance_output, list)
    assert isinstance(stations_by_distance_output[0], tuple)
def test_stations_within_radius():
    stations = build_station_list()
    stations_within_1k_of_origin = []
    assert stations_within_1k_of_origin == stations_within_radius(stations, (0, 0), 0)
    assert isinstance(stations_within_radius(stations, (52.2333, 0.0833), 10), list)

def test_rivers_with_stations():
        assert isinstance(rivers_with_stations(build_station_list()), set)

def test_stations_by_river():
    assert isinstance(stations_by_river(build_station_list()), dict)

def test_rivers_by_station_number():
    rivers_by_station_number_output = rivers_by_station_number(build_station_list(), 10)
    assert isinstance(rivers_by_station_number_output, list)
    assert isinstance(rivers_by_station_number_output[0], tuple)
    assert len(rivers_by_station_number_output) >= 10

test_hav()
test_detograd()
test_distance()
test_stations_by_distance()
test_stations_within_radius()
test_rivers_with_stations()
test_stations_by_river()
test_rivers_by_station_number()