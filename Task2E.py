from plot import *
from flood import *
from floodsystem.stationdata import *

stations = build_station_list()

current_highest = [i[0] for i in stations_highest_rel_level(stations, 5)]

for i in current_highest:
	dates, levels = fetch_measure_levels(i.measure_id, datetime.timedelta(days=10))
	plot_water_levels(i, dates, levels)