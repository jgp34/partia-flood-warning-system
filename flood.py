from floodsystem.station import *
from floodsystem.stationdata import *

def stations_level_over_threshold(stations, tol):
	update_water_levels(stations)
	stations_over_threshold = []
	for i in stations:
		if i.relative_water_level()[1] != None:
			if i.relative_water_level()[1] > tol:
				stations_over_threshold.append( (i, i.relative_water_level()[1]) )
				
	return stations_over_threshold
	
def stations_highest_rel_level(stations, N):
	update_water_levels(stations)
	tpls = []
	for i in stations:
		if i.relative_water_level()[1] != None:
			tpls.append( (i, i.relative_water_level()[1]) )
	
	tpls.sort(key=lambda tup: tup[1], reverse=True)
	return tpls[0:N]